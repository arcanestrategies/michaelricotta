<?php
/**
 * Michael Ricotta back compat functionality
 *
 * Prevents Michael Ricotta from running on WordPress versions prior to 4.1,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.1.
 *
 * @package WordPress
 * @subpackage Michael_Ricotta
 * @since Michael Ricotta 1.0
 */

/**
 * Prevent switching to Michael Ricotta on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since Michael Ricotta 1.0
 */
function michaelricotta_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'michaelricotta_upgrade_notice' );
}
add_action( 'after_switch_theme', 'michaelricotta_switch_theme' );

/**
 * Add message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Michael Ricotta on WordPress versions prior to 4.1.
 *
 * @since Michael Ricotta 1.0
 */
function michaelricotta_upgrade_notice() {
	$message = sprintf( __( 'Michael Ricotta requires at least WordPress version 4.1. You are running version %s. Please upgrade and try again.', 'michaelricotta' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevent the Customizer from being loaded on WordPress versions prior to 4.1.
 *
 * @since Michael Ricotta 1.0
 */
function michaelricotta_customize() {
	wp_die( sprintf( __( 'Michael Ricotta requires at least WordPress version 4.1. You are running version %s. Please upgrade and try again.', 'michaelricotta' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'michaelricotta_customize' );

/**
 * Prevent the Theme Preview from being loaded on WordPress versions prior to 4.1.
 *
 * @since Michael Ricotta 1.0
 */
function michaelricotta_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'Michael Ricotta requires at least WordPress version 4.1. You are running version %s. Please upgrade and try again.', 'michaelricotta' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'michaelricotta_preview' );
