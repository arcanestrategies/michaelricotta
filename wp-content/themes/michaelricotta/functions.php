<?php
/**
 * Michael Ricotta functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Michael_Ricotta
 * @since Michael Ricotta 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Michael Ricotta 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
 * Michael Ricotta only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'michaelricotta_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Michael Ricotta 1.0
 */
function michaelricotta_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on michaelricotta, use a find and replace
	 * to change 'michaelricotta' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'michaelricotta', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'michaelricotta' ),
		'social'  => __( 'Social Links Menu', 'michaelricotta' ),
		'footer'  => __( 'Footer Menu', 'michaelricotta' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	$color_scheme  = michaelricotta_get_color_scheme();
	$default_color = trim( $color_scheme[0], '#' );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'michaelricotta_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', michaelricotta_fonts_url() ) );

	add_image_size( 'featured-press', 150 );
	add_image_size( 'company-logo-xs', 9999, 12 );
	add_image_size( 'portfolio', 260 );
}
endif; // michaelricotta_setup
add_action( 'after_setup_theme', 'michaelricotta_setup' );

/**
 * Register widget area.
 *
 * @since Michael Ricotta 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function michaelricotta_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'michaelricotta' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'michaelricotta' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'michaelricotta_widgets_init' );

if ( ! function_exists( 'michaelricotta_fonts_url' ) ) :
/**
 * Register Google fonts for Michael Ricotta.
 *
 * @since Michael Ricotta 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function michaelricotta_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Noto Sans, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'michaelricotta' ) ) {
		$fonts[] = 'Noto Sans:400italic,700italic,400,700';
	}

	/* translators: If there are characters in your language that are not supported by Noto Serif, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'michaelricotta' ) ) {
		$fonts[] = 'Noto Serif:400italic,700italic,400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'michaelricotta' ) ) {
		$fonts[] = 'Inconsolata:400,700';
	}

	/* translators: To add an additional character subset specific to your language, translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language. */
	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'michaelricotta' );

	if ( 'cyrillic' == $subset ) {
		$subsets .= ',cyrillic,cyrillic-ext';
	} elseif ( 'greek' == $subset ) {
		$subsets .= ',greek,greek-ext';
	} elseif ( 'devanagari' == $subset ) {
		$subsets .= ',devanagari';
	} elseif ( 'vietnamese' == $subset ) {
		$subsets .= ',vietnamese';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), '//fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Enqueue scripts and styles.
 *
 * @since Michael Ricotta 1.0
 */
function michaelricotta_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'michaelricotta-fonts', michaelricotta_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	// wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our main stylesheet.
	wp_enqueue_style( 'michaelricotta-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'michaelricotta-ie', get_template_directory_uri() . '/css/ie.css', array( 'michaelricotta-style' ), '20141010' );
	wp_style_add_data( 'michaelricotta-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'michaelricotta-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'michaelricotta-style' ), '20141010' );
	wp_style_add_data( 'michaelricotta-ie7', 'conditional', 'lt IE 8' );

	// wp_enqueue_script( 'michaelricotta-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'michaelricotta-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}

	// wp_enqueue_script( 'michaelricotta-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20141212', true );
	wp_localize_script( 'michaelricotta-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'michaelricotta' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'michaelricotta' ) . '</span>',
	) );
}
add_action( 'wp_enqueue_scripts', 'michaelricotta_scripts' );

/**
 * Add featured image as background image to post navigation elements.
 *
 * @since Michael Ricotta 1.0
 *
 * @see wp_add_inline_style()
 */
function michaelricotta_post_nav_background() {
	if ( ! is_single() ) {
		return;
	}

	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	$css      = '';

	if ( is_attachment() && 'attachment' == $previous->post_type ) {
		return;
	}

	if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
		$prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
			.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
			.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	if ( $next && has_post_thumbnail( $next->ID ) ) {
		$nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); }
			.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
			.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	wp_add_inline_style( 'michaelricotta-style', $css );
}
add_action( 'wp_enqueue_scripts', 'michaelricotta_post_nav_background' );

/**
 * Display descriptions in main navigation.
 *
 * @since Michael Ricotta 1.0
 *
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function michaelricotta_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'michaelricotta_nav_description', 10, 4 );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since Michael Ricotta 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function michaelricotta_search_form_modify( $html ) {
	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'michaelricotta_search_form_modify' );

/**
 * Implement the Custom Header feature.
 *
 * @since Michael Ricotta 1.0
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since Michael Ricotta 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since Michael Ricotta 1.0
 */
require get_template_directory() . '/inc/customizer.php';

add_action( 'init', 'press_and_media' );
function press_and_media() {
  register_post_type( 'media_appearances',
    array(
      'labels' => array(
        'name' => __( 'Press & Media' ),
        'singular_name' => __( 'Press' )
      ),
      'public' => true,
      'has_archive' => false,
	  'show_in_admin_bar' => true,
	  //'menu_icon' => 'get_template_directory_uri() . "images/cutom-posttype-icon.png"',
	  'capability_type' => 'post',
	  'supports' => array(
			'title',
			'thumbnail',
			'custom-fields',
		)
    )
  );
  register_taxonomy(
		'sources',
		'media_appearances', // 'post',
		array(
			'label' => __( 'Media Source' ),
			'rewrite' => array( 'slug' => 'source' ),
			/*'capabilities' => array(
				'assign_terms' => 'edit_guides',
				'edit_terms' => 'publish_guides'
			)*/
		)
	);
}

add_action( 'init', 'stat_rat' );
function stat_rat() {
  register_post_type( 'career_stats',
    array(
      'labels' => array(
        'name' => __( 'Career Stats' ),
        'singular_name' => __( 'Stat' )
      ),
      'public' => true,
      'has_archive' => false,
	  'show_in_admin_bar' => true,
	  //'menu_icon' => 'get_template_directory_uri() . "images/cutom-posttype-icon.png"',
	  'capability_type' => 'post',
	  'supports' => array(
			'title',
			'excerpt',
		)
    )
  );
  register_taxonomy(
		'stats',
		'career_stats', // 'post',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'stats' ),
		)
	);
}

add_action( 'init', 'award_method' );
function award_method() {
  register_post_type( 'awards',
    array(
      'labels' => array(
        'name' => __( 'Awards' ),
        'singular_name' => __( 'Award' )
      ),
      'public' => true,
      'has_archive' => false,
	  'show_in_admin_bar' => true,
	  //'menu_icon' => 'get_template_directory_uri() . "images/cutom-posttype-icon.png"',
	  'capability_type' => 'post',
	  'supports' => array(
			'title',
			'excerpt',
			'thumbnail',
			'custom-fields',
		)
    )
  );
}

add_action( 'init', 'accomplish_method' );
function accomplish_method() {
  register_post_type( 'accomplishments',
    array(
      'labels' => array(
        'name' => __( 'Accolades' ),
        'singular_name' => __( 'Accomplishment' )
      ),
      'public' => true,
      'has_archive' => false,
	  'show_in_admin_bar' => true,
	  //'menu_icon' => 'get_template_directory_uri() . "images/cutom-posttype-icon.png"',
	  'capability_type' => 'post',
	  'supports' => array(
			'title',
			'excerpt',
			'thumbnail',
			//'custom-fields',
		)
    )
  );
}

add_action( 'init', 'portfolio_method' );
function portfolio_method() {
  register_post_type( 'portfolio',
    array(
      'labels' => array(
        'name' => __( 'Portfolio' ),
        'singular_name' => __( 'Portfolio' )
      ),
      'public' => true,
      'has_archive' => false,
	  'show_in_admin_bar' => true,
	  //'menu_icon' => 'get_template_directory_uri() . "images/cutom-posttype-icon.png"',
	  'capability_type' => 'post',
	  'supports' => array(
			'title',
			//'excerpt',
			'thumbnail',
			'custom-fields',
		)
    )
  );
  register_taxonomy(
		'company',
		'portfolio', // 'post',
		array(
			'label' => __( 'Company' ),
			'rewrite' => array( 'slug' => 'stats' ),
		)
	);
}

function remove_ul ( $menu ){
    return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );
}
add_filter( 'wp_nav_menu', 'remove_ul' );

function tip_plugin_get_terms($term_id) {
	$associations = taxonomy_image_plugin_get_associations();
	$tt_id = absint( $term_id );
	$img_id = false;
	if ( array_key_exists( $tt_id, $associations ) ) {
		$img_id = absint( $associations[$tt_id] );
	}
	return $img_id;
}
