$(document).ready(function() {
  var $container = $('.port');
  var masonryOptions = {
    /*columnWidth: '.port li',*/
    itemSelector: 'li'
  }
  $container.imagesLoaded(function() {
    $container.masonry(masonryOptions);
  });
  // initialize
  $container.masonry(masonryOptions);
  var items = new Array();
  var these = new Array();
  items.push($('.port li'));
  //var msnry = $container.data('masonry');
  $('.filter').click(function() {
    var these = $(this).attr('id');
    $('.port li').each(function() {
      if (!$(this).hasClass(these)) {
        $(this).hide();
      }
    });
    $('.port .' + these).show();
    $container.masonry('layout').masonry(masonryOptions);
  });
  $('.slow a').click(function() {
    $('html, body').animate({
      scrollTop: $($.attr(this, 'href')).offset().top - 260
    }, 600);
    if ($('#nav').data('size') == 'big') {
      $('#nav').stop().animate({
        height: '60px'
      }, 300);
      $('#nav').data('size', 'small');
    };
    return false;
  });
  $('#nav').data('size', 'small');
  $('.hamburger').click(function() {
    if ($('#nav').data('size') == 'small') {
      $('#nav').stop().animate({
        height: '279px'
      }, 300);
      $('#nav').data('size', 'big');
    } else {
      $('#nav').stop().animate({
        height: '60px'
      }, 300);
      $('#nav').data('size', 'small');
    }
  });
  if ($('html').width() >= 480) {
    var orientpoint = 'horizontal';
  } else {
    var orientpoint = 'vertical';
  }
  $('#stats').accordionPro({
    autoPlay: true,
    startClosed: false,
    orientation: orientpoint,
    /*orientation: 'vertical',*/
    firstSlide: 1,
    horizontalWidth: 900,
    horizontalHeight: 280,
    theme: 'dark',
    rounded: false,
		verticalSlideHeight : 'fitToContent',
    /*onSlideOpen : callback,
    onSlideClose : callback,*/
  }).find('span:first').show();
  /*$('#nav').data('size','big');*/
  $('#featured').data('size', 'big');
  ftheight = $('#featured').height();
  /*mnheight = $('#nav').height();*/
  fontz = $('#featured span').css("fontSize");
  /*stwidth = $('#stats').width();*/
});
var onViewportChange = function() {
  if ($('html').width() >= 768) {
    $('#nav').data('size', 'small');
    $('#nav').height(60);
  }
};
var onViewportOrient = function() {
  if ($('html').width() >= 480) {
    var orientpoint = 'horizontal';
  } else {
    var orientpoint = 'vertical';
  }
};
$(window).on({
  orientationchange: function() {
    onViewportOrient();
    location.reload();
  },
  resize: function() {
    onViewportChange();
    onViewportOrient();
  }
});
$(window).scroll(function() {
  if ($(document).scrollTop() > 0) {
    if ($('#featured').data('size') == 'big') {
      $('#featured').data('size', 'small');
      if ($('html').width() >= 768) {
        $('#featured').stop().animate({
          height: '60px'
        }, 300);
      } else {
        $('#featured').stop().animate({
          height: '25px'
        }, 300);
      }
      $('#featured span').stop().animate({
        fontSize: '0px',
      }, 300, function() {
        $('#featured span').hide();
      });
      $('#featured li img').stop().animate({
        width: '40%'
      }, 300);
    };
    /*if($(document).scrollTop() > 190) {
        $('#greetings').stop().animate({
            height: '80px',
			marginLeft: '-10000px'
        },1);
	} else {
        $('#greetings').stop().animate({
			marginLeft: '0px'
        },1, function(){
			$('#greetings').height('auto');
		});
	};*/
    /*$('#stats').stop().animate({
            width: '100%'
        },300, function(){
			$('#about').attr('background','#fff');
		});*/
  } else {
    if ($('#featured').data('size') == 'small') {
      $('#featured').data('size', 'big');
      $('#featured').stop().animate({
        height: ftheight
      }, 300);
      $('#featured span').stop().animate({
        fontSize: fontz,
      }, 300);
      $('#featured span').show();
      $('#featured li img').stop().animate({
        width: '100%'
      }, 300);
    }
  }
});
