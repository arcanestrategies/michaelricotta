<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Michael_Ricotta
 * @since Michael Ricotta 1.0
 */

get_header(); 
?>
	<div id="featured">
			<ul class="wrapper">
				<li>
					<a href="#press-elitedaily"  class="press slow">
					<span>Exclusive Interviews on Elite Daily</span>
					<img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/elite_daily.png" />
					</a>
				</li>
				<li>
					<a href="#press-fox"  class="press slow">
					<span>Tech Correspondent, Fox Business</span>
					<img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/fox.png" />
					</a>
				</li>
				<li>
					<a href="#press-cnbc"  class="press slow">
					<span>Tech Correspondent, CNBC Power Lunch</span>
					<img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/cnbc.png" />
					</a>
				</li>
			</ul>
	</div>
<div class="cleartop">
	<div id="about" class="wrapper">
		<div class="content">
			<div id="greetings">
					<h1>Michael Ricotta</h1>
					<h2>Web Solutions Engineer</h2>
					<!--h3>8+ years industry experience, B.A., Director\VP Roles</h3-->
					<h3>9 years Industry Experience, B.A., CEH</h3>
			</div>
			<div id="stats">
				<ol>
					<li>
						<h5><span>Highlights</span></h5>
						<div><ul>
							<li><span>Trilingual:</span> English, Spanish, Italian</li>
							<li><span>Team Size:</span> Range 10 - 35; <span>Managed Nationalities:</span> 9</li>
							<li><span>Experience:</span> 8+ years<br/>Design, IA, Project Management, Systems Administration, Development, Customer Services</li>
							<li><span>Education:</span> Bachelor of Arts, College of The Holy Cross<!--, History &amp; Visual Arts--></li>
						</ul>
						</div>
					</li>
					<li>
						<h5><span>DevOps</span></h5>
						<div><ul>
							<li><span>Supported Stacks:</span> LAMP, LEMP (nginx), WAMP, WIMP (IIS), XAMPP</li>
							<li>Debian &amp; RPM based distros (CentOS, RedHat, Ubuntu, Debian), &amp; Windows 2003+</li>
							<li><span>Network Config:</span> SMTP &amp; Exchange servers, DNS, Active Directory</li>
							<li><span>Deployment and Source Control:</span> Ansible, Celery (RabbitMQ), Puppet, Git, SVN, Mercurial, BitBucket.</li>
							<li><span>SOP's:</span> Agile and Waterfall sprint and release cycles, Emergency response</li>
						</ul>
						</div>
					</li>
					<li>
						<h5><span>Databases</span></h5>
						<div><ul>
							<li><span>Relational DB Engines:</span> MySQL, PostgreSQL, SQLite, and MsSQL</li>
							<li><span>Other DB Management and Markup:</span> MariaDB, MongoDB, Memcached, XML</li>
							<li><span>ERP Integrations:</span> DB2/AS/400, Quickbooks</li>
							<li><span>CRM Integrations:</span> Salesforce, ACT</li>
						</ul>
						</div>
					</li>
					<li>
						<h5><span>Languages</span></h5>
						<div><ul>
							<li><span>Server-Side</span> PHP (OOP &amp; Procedural), Python, Javascript (Node.js), .NET, Bash, CMD, Java, Objective C</li>
							<li><span>Client-Side</span> HTML 5, CSS3, RSS, JavaScript (Angular, Prototype, JQuery), Flash</li>
						</ul>
						</div>
					</li>
					<li>
						<h5><span>Frameworks</span></h5>
						<div><ul>
							<li><span>PHP:</span> WordPress, Drupal, Typo3, Joomla, Oscommerce, Magento, Laravel</li>
							<li><span>Python:</span> Django</li>
							<li><span>Java:</span> Android SDK</li>
							<li><span>ObjectiveC:</span> iOS SDK</li>
						</ul>
						</div>
					</li>
					<li>
						<h5><span>Background</span></h5>
						<div><ul>
							<li><span>Experienced In:</span> Design and planning to development and systems administration.</li>
							<li><span>Industries Supported:</span> Small businesses to large Bio-Pharma</li>
							<li><span>Additional Info:</span> Advanced security and encryption protocols, injection testing, OWASP, Gov&apos;t 508 compliance, PCI compliance, FDA 21 CFR 11 compliance.</li>
							<li><span>Other Tech:</span> Virtualization, Hardware build, Arduino, Raspberry Pi</li>
						</ul>
						</div>
					</li>
				</ol>
			</div>
			</div>
		</div>
	</div>
	<a class="anchor slow" href="#portfolio"></a>
	<h4>In The Press</h4>
	<div id="press" class="wrapper">
		<ul>
			<li id="press-elitedaily" class="elitepost" >
				<a href="http://elitedaily.com/news/politics/cybersecurity-expert-obamacare-hacked/743878/" target="_blank">In Exclusive Interview, Cybersecurity Expert Explains How HealthCare.Gov Got Hacked</a>
			</li>
			<li id="press-cnbc" class="cnbcpost">
				<a href="http://video.cnbc.com/gallery/?video=3000205461" target="_blank">Obamacare: Volume really to blame</a>
			</li>
			<li id="press-cnbc" class="cnbcpost">
				<a href="http://video.cnbc.com/gallery/?video=3000207604" target="_blank">Obamacare: Fixable</a>
			</li>
			<li id="press-fox"  class="foxpost">
				<a href="http://www.foxnews.com/tech/2014/09/11/beware-cyber-risks-before-dumping-old-phone/" target="_blank">
					Beware of cyber risks before dumping your old phone
				</a>
			</li>
		</ul>
	</div>	
	<a class="anchor slow" href="#awards"></a>
	<h4>Portfolio</h4>
	<div id="portfolio" class="wrapper">
			<div id="filters">
				<span id="all" class="filter">All</span>
				<span id="consulting" class="filter">Arcane Strategies</span>
				<span id="bfm" class="filter">Blue Fountain Media</span>
				<span id="axxiem" class="filter">Axxiem Web Solutions</span>
			</div>
			<ul class="port wrapper">
				<li class="consulting bfm axxiem all"><a href="http://www.cornerstone.it" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/cornerstone.png" /></a></li>
				<li class="bfm all"><a href="http://www.altibase.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/altibase.png" /></a></li>
				<li class="bfm all"><a class="transparentimageblack" href="http://ads.aolonnetwork.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/aolon.png" /></a></li>
				<li class="bfm all"><a href="http://www.benihanaworld.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/benihana.png" /></a></li>
				<li class="bfm all"><a href="http://blueibis.bfmdev9.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/balancz.png" /></a></li>
				<li class="bfm all"><a href="http://cbps.canon.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/canon.png" /></a></li>
				<li class="bfm all"><a href="http://www.pubcrawlnewyork.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/pubcrawl.png" /></a></li>
				<li class="bfm all"><a href="http://www.starkcarpet.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/stark.png" /></a></li>
				<li class="bfm all"><a href="http://www.whowasbookseries.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/penguin.png" /></a></li>
				<li class="bfm all"><a href="http://www.csnews.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/stagnito.png" /></a></li>
				<li class="bfm all"><a href="http://www.baldoorfoods.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/baldor.png" /></a></li>
				<!--li class="bfm"><a href="http://www.johnjay.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/johnjay.png" /></a></li-->
				<li class="axxiem all"><a href="http://www.allcells.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/allcells.png" /></a></li>
				<li class="axxiem all"><a href="http:://www.axxitrials.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/axxitrials.png" /></a></li>
				<li class="axxiem all"><a href="http://www.choruspharma.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/lilly.png" /></a></li>
				<li class="axxiem all"><a href="http://www.reagentproteins.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/reagent.png" /></a></li>
				<li class="axxiem all"><a href="http://www.regeneron.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/regeneron.png" /></a></li>
				<li class="consulting all"><a href="http://www.arcanestrategies.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/arcanestrategies.png" /></a></li>
				<li class="consulting all"><a href="http://www.jrlsurveying.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/jrlsurveying.gif" /></a></li>
				<li class="consulting all"><a class="transparentimage" href="http://www.prismabanners.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/prismabanners.png"></a></li>
				<li class="consulting all"><a href="http://www.stephenandolino.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/stephenandolino.png" /></a></li>
				<li class="consulting all"><a href="http://www.yellowfinnavigation.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/yellowfin.png" /></a></li>
				<li class="consulting all"><a href="http://www.kathymuir.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/kathymuir.gif" /></a></li>
				<li class="consulting all"><a href="http://www.dadstie.com" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/portfolio/dadstielogo.png" /></a></li>
			</ul>
	</div>
	<a class="anchor slow" href="#contact"></a>
	<h4>Awards</h4>
	<div id="awards">
		<ul class="wrapper">
			<li><a href="http://www.webaward.org/winner.asp?eid=14978#.U-l0GfldU6w" target="_blank"><span class="awardname">WebAward</span><br/> Biotechnology &amp; Technology Standard of Excellence Web Award: Winner	2010<br/>
			<img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/webaward-trophy_10.png" />
			</a></li>
			<li><a href="http://www.webaward.org/winner.asp?eid=17424#.U-l0F_ldU6x" target="_blank"><span class="awardname">WebAward</span><br/> Biotechnology Ecommerce Standard of Excellence Web Award: Winner	 2011<br/>
			<img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/webaward-trophy_11.png" /></a></li>
			<li><a href="http://www.interactivemediaawards.com/winners/certificate.asp?param=282750&cat=1" target="_blank"><span class="awardname">Interactive Media Awards</span><br/> Outstanding Achievement - Restaurant: Silver	2013<br/>
			<img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/ima-logo.png" /></a></li>
			<li><a href="http://www.daveyawards.com/winners/list/?l=B&event=9&category=12&award=S&form=2" target="_blank"><span class="awardname">Davey Awards</span><br/> Entertainment Mobile Websites: Silver	2013<br/>
			<img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/davey-trophy.png" /></a></li>
		</ul>
	</div>
	<a class="anchor slow" href="#footer"></a>
	<h4>Contact Me</h4>
	<div id="contact" class="wrapper">
		<form method="post" action="mail.php" name="contactform">
			<div><input placeholder="First Last" type="text" name="cname" autocomplete="on" value="" pattern=".{2,}" required title="I'm gonna need more letters than that ;)"><label for="cname" >Name:</label></div>
			<div><input placeholder="email@domain.com" type="email" name="cemail" value="" autocomplete="on" pattern=".{5,}" required title="I'm gonna need more letters than that ;)"><label for="cemail" >Email:</label></div>
			<div><textarea rows="5" cols="" name="cmessage" value="" pattern=".{15,}" required title="Please elaborate"></textarea><label for="cmessage" >Message:</label></div>
			<div><input type="submit" name="submit" value="Submit"></div>
			<div id="message"></div>
		</form>
		<div id="aboutme">
					<ul>
						<li>
							<span>Eagle Scout, Westchester-Putnam Council</span>
							<img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/eagle_scout.png" />
						</li>
						<li>
							<span>9-time NCAA D1 medalist, school record holder</span>
							<img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/NCAA.png" />
						</li>
						<li>
							<span>BA, College of The Holy Cross</span>
							<img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/holycross.png" />
						</li>
						<li>
							<span>CEO, Founder of Arcane Strategies</span>
							<img src="<?php echo esc_url( get_template_directory_uri() ) ?>/images/arcane.png" />
						</li>
					</ul>
		</div>
	</div>
<?php get_footer(); ?>