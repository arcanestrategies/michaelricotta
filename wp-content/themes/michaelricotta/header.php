<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Michael_Ricotta
 * @since Michael Ricotta 1.0
 */
?><!DOCTYPE html>
<html>
<head>
	<title>Web Technology Expert | Michael Ricotta</title>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ) ?>/style.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ) ?>/css/accordionpro.css">
	<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=no">
	<meta name="robots" content="index, follow">
	<!--link rel="stylesheet" href="/css/bootstrap.min.css"-->
	<!--link rel="stylesheet" href="/css/bootstrap-responsive.min.css"-->
	<?php wp_head(); ?>
</head>
<body>
<?php include_once("analyticstracking.php") ?>
	<div id="nav">
		<ul>
			<li>
				<ul class="mobile_super">
					<li><a href="https://www.linkedin.com/pub/mike-ricotta/b/6b9/a6a" target="_blank"><span class="linkedin social">linkedin</span></a></li>
					<li><a href="https://twitter.com/MichaelRicotta" target="_blank"><span class="twitter social">twitter</span></a></li>
					<li><a href="https://bitbucket.org/mtrico08" target="_blank"><span class="bitbucket social">bitbucket</span></a></li>
					<li><a href="#contact" class="slow"><span class="contact social" >contact me</span></a></li>
				</ul>
				<div class="hamburger sprite"></div>
			</li>
			<?php
			echo wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', ) );
			?>
			<li><a href="https://www.linkedin.com/pub/mike-ricotta/b/6b9/a6a" target="_blank"><span class="linkedin social">linkedin</span></a></li>
			<li><a href="https://twitter.com/MichaelRicotta" target="_blank"><span class="twitter social">twitter</span></a></li>
			<li><a href="https://bitbucket.org/mtrico08" target="_blank"><span class="bitbucket social">bitbucket</span></a></li>
			<li><a href="#contact" class="slow"><span class="contact social" >contact me</span></a></li>
		</ul>
	</div>
	<div id="featured">
			<ul class="wrapper">
			<?php
			$args = array(
				'orderby' => 'count',
				'order'   => 'DESC',
				'number' => 5,
				'hide_empty' => 1,
			);
			$terms = get_terms( 'sources', $args );
			foreach ($terms as $term):
				$tip = tip_plugin_get_terms($term->term_id);
				?>
				<li>
					<a href="#press" class="press slow press-<?php echo $term->term_id;?>">
						<span><?php echo $term->description; ?></span>
						<img src="<?php echo reset(wp_get_attachment_image_src( $tip, 'featured-press' )); ?>" alt="<?php echo $term->description; ?>"/>
					</a>
				</li>
			<?php endforeach; ?>
			</ul>
	</div>
