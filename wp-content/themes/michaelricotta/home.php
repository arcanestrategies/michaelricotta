<?php
/*
Template Name: Home
*/

get_header(); 
?>
<div class="cleartop">
	<div id="about" class="wrapper">
		<div class="content">
			<div id="greetings">
					<h1>Michael Ricotta</h1>
					<h2>CEO Arcane Strategies</h2>
					<a href="http://www.arcanestrategies.com" target="_blank">www.arcanestrategies.com</a>
					<h3>Web Solutions Engineer, B.A, CEH</h3>
					<p class="font-medium">Mike is a veteran of Madison Avenue digital media, with 14+ years of professional experience.  As an expert in digital media, Mike has been a partner in founding several web and apps products, has built 3 international development teams through acquisition, and appeared on media networks and publications such as CNBC, Fox Business, Bloomberg, Business Insider, and CIO Magazine, among others.</p>
					<!--h3>8+ years industry experience, B.A., Director\VP Roles</h3-->
			</div>
			<div id="stats">
				<ol>
				<?php
				$args = array(
					'hide_empty' => 1,
					'orderby' => 'count',
					'order' => 'DESC',
				);
				$terms = get_terms( 'stats', $args ); 
				foreach ($terms as $term) {
					$postargs = array( 'post_type' => 'career_stats',
						'tax_query' => array(
							array(
								'taxonomy' => 'stats',
								'field' => 'slug',
								'terms' => $term->slug,
							),
						),					
					);
					$loop = new WP_Query( $postargs );
					while ( $loop->have_posts() ) : $loop->the_post();
							$output .= '<li><span>'.get_the_title().': </span>'.get_the_excerpt().'</li>';
					endwhile;
					$line = '<li>'
							.'<h5><span>'.$term->name.'</span></h5>'
							.'<div><ul>'
							.$output
							.'</div></ul>'
							.'</li>';
					echo $line;
					$output = '';
				};
				?>
				</ol>
			</div>
		</div>
	</div>
	<span class="slow"><a class="anchor" href="#portfolio"></a></span>
	<h4>In The Press</h4>
	<div id="press"><div class="wrapper">
		<ul>
			<?php 
			$args = array( 'post_type' => 'media_appearances', 'posts_per_page' => 20 );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post();
				$terms = wp_get_post_terms( $post->ID, 'sources', array("fields"=>"all") );
				$tax = '';
				foreach ($terms as $term) {
					$tax .= 'press-'.$term->term_id.' ';
					$tip = tip_plugin_get_terms($term->term_id);
				}
			?>
				<li class="<?php echo $tax;?>" >
					<span class="source_icon_small">
						<img src="<?php echo reset(wp_get_attachment_image_src( $tip, 'company-logo-xs' )); ?>" alt="<?php the_title(); ?>">
					</span>
					<a href="<?php echo get_post_meta($post->ID, 'Source URL', true); ?>" target="_blank" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</li>
			<?php endwhile; ?>
		</ul>
	</div></div>	
	<span class="slow"><a class="anchor" href="#awards"></a></span>
	<h4>Portfolio</h4>
	<div id="portfolio" class="wrapper">
			<div id="filters">
			<span id="all" class="filter">All</span>
			<?php	
			$args = array(
				'orderby' => 'count',
				'order'   => 'DESC',
				'number' => 5,
				'hide_empty' => 1,
			);
			$terms = get_terms( 'company', $args );
			foreach ($terms as $term) {
				$line = '<span id="'.$term->slug.'" class="filter">'.$term->name.'</span>';
				echo $line;
			};
			?>
			</div>
			<ul class="port wrapper">
				<?php 
					$args = array( 'post_type' => 'portfolio', 'posts_per_page' => 1000, 'orderby' => 'count', 'order' => 'DESC' );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post();
						$terms = wp_get_post_terms( $post->ID, 'company', array("fields"=>"all") );
						$tax = '';
						foreach ($terms as $term) {
								$tax .= $term->slug.' ';
							}
						//$url = wp_get_attachment_url(get_post_thumbnail_id($post->ID,'portfolio'));
						?>
						<li class="all <?php echo $tax; ?>">
							<a href="<?php echo get_post_meta($post->ID, 'Source URL', true); ?>" target="_blank" title="<?php the_title();?>">
								<?php the_post_thumbnail('portfolio'); ?>
							</a>
						</li>
					<?php endwhile; 
				?>
			</ul>
	</div>
	<span class="slow"><a class="anchor" href="#contact"></a></span>
	<h4>Awards</h4>
	<div id="awards">
		<ul class="wrapper">
			<?php 
			$awargs = array( 'post_type' => 'awards', 'posts_per_page' => 20 );
			$loop = new WP_Query( $awargs );
			while ( $loop->have_posts() ) : $loop->the_post();
				//$url = wp_get_attachment_url(get_post_thumbnail_id($post->ID,'featured-press'));
				?>
				<li>
					<a href="<?php echo get_post_meta($post->ID, 'Source URL', true); ?>" target="_blank">
						<span class="awardname"><?php the_title(); ?></span>
						<br/>
						<?php the_post_thumbnail('featured-press'); ?>
						<br/>
						<?php the_excerpt();?>
					</a>
				</li>
			<?php endwhile; ?>
		</ul>
	</div>
	<span class="slow"><a class="slow" href="#footer"></a></span>
	<h4>Contact Me</h4>
	<div id="contact" class="wrapper">
		<?php
			$referrer = wp_get_original_referer();
			if(isset($referrer)&&strpos($referrer,'upwork')!== false){
				echo 'Sorry, Upwork policy requires that Upwork customers must contact me through Upwork.';
			} else {
				echo do_shortcode(' [contact-form-7 id="188" title="Contact"] ');
			}
		?>
		<!--form method="post" action="mail.php" name="contactform">
			<div><input placeholder="First Last" type="text" name="cname" autocomplete="on" value="" pattern=".{2,}" required title="I'm gonna need more letters than that ;)"><label for="cname" >Name:</label></div>
			<div><input placeholder="email@domain.com" type="email" name="cemail" value="" autocomplete="on" pattern=".{5,}" required title="I'm gonna need more letters than that ;)"><label for="cemail" >Email:</label></div>
			<div><textarea rows="5" cols="" name="cmessage" value="" pattern=".{15,}" required title="Please elaborate"></textarea><label for="cmessage" >Message:</label></div>
			<div><input type="submit" name="submit" value="Submit"></div>
			<div id="message"></div>
		</form-->
		<div id="aboutme">
					<ul class="wrapper">
						<?php 
						$accs = array( 'post_type' => 'accomplishments', 'posts_per_page' => 20 );
						$loop = new WP_Query( $accs );
						while ( $loop->have_posts() ) : $loop->the_post();
							//$url = wp_get_attachment_url(get_post_thumbnail_id($post->ID,'featured-press'));
							?>
							<li>
								<?php the_post_thumbnail('featured-press'); ?>
								<br>
								<span><?php the_title();?></span>
							</li>
						<?php endwhile; ?>
					</ul>
		</div>
	</div>
<?php get_footer(); ?>
