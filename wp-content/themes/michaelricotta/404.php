<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div class="cleartop">
	<div class="wrapper">
		<div class="content four-o-four">
			Whoops!  No page here.  Try the navigation menus.
		</div>
	</div>
</div>
<?php get_footer(); ?>
