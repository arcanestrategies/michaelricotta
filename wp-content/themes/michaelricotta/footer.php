<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Michael_Ricotta
 * @since Michael Ricotta 1.0
 */
?>
	<div id="footer">
		<div class="wrapper">
			<ul class="icons right opensource">
					<span class="footer_title">Open Source Projects</span>
					<li><a href="https://www.openhub.net/p/php/contributors" target="_blank"><span class="php social">php</a></li>
					<li><a href="https://developers.facebook.com" target="_blank"><span class="facebook social">facebook</span></a></li>
					<li><a href="http://codex.wordpress.org/Contributing_to_WordPress" target="_blank"><span class="wordpress social">wordpress</span></a></li>
					<li><a href="https://developers.google.com/cast/" target="_blank"><span class="chrome social">chromecast</span></a></li>
					<li><a href="https://www.youtube.com/user/topcontributors" target="_blank"><span class="youtube social">youtube</span></a></li>
					<li><a href="http://nodejs.org/community/" target="_blank"><span class="nodejs social">nodejs</span></a></li>
					<li><a href="http://developer.android.com/" target="_blank"><span class="android social">android</span></a></li>
				</li>
			</ul>
			<ul class="icons right sociallinks">
			<span class="footer_title">Follow Me</span>
				<li><a href="http://www.linkedin.com/pub/mike-ricotta/b/6b9/a6a" target="_blank"><span class="linkedin social">linkedin</a></li>
				<li><a href="https://twitter.com/MichaelRicotta" target="_blank"><span class="twitter social">twitter</span></a></li>
				<li><a href="https://bitbucket.org/mtrico08" target="_blank"><span class="bitbucket social">bitbucket</span></a></li>
			</ul>
			<ul class="nav">
			<?php
			echo wp_nav_menu( array( 'theme_location' => 'footer', 'container' => '', ) );
			?>
			</ul>
			<span class="copy clear">&copy;<?php echo date("Y"); ?> Michael Ricotta</span>
		</div>
	</div>
</div>
<footer>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="<?php echo esc_url( get_template_directory_uri() ) ?>/js/jquery.accordionpro.js"></script>
	<script src="<?php echo esc_url( get_template_directory_uri() ) ?>/js/masonry.pkgd.min.js"></script>
	<script src="<?php echo esc_url( get_template_directory_uri() ) ?>/js/scripts.js"></script>
</footer>
<?php wp_footer(); ?>
</body>
</html>
